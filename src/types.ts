export interface IPicture {
  medium?: string,
  original?: string
}

export interface ICountry {
  name: string,
  code: string,
  timezone: string
}

export interface IShow {
  averageRuntime: number
  dvdCountry: any
  ended: any
  externals: any
  genres: [string]
  id: number
  image: IPicture
  language: string
  name: string
  network: {
    id: number,
    name: string,
    country?: ICountry
  }
  officialSite: string
  premiered: string
  rating: {
    average: number
  }
  runtime: number
  schedule?: any
  status: string
  summary: string
  type?: string
  updated: number
  url: string
  webChannel: any
  weight: any
}

export interface ICast {
  character: {
    id: number
    image: IPicture
    name: string
    url: string
  }
  person: {
    birthday: "1990-01-14"
    country: ICountry
    deathday?: string
    gender: string
    id: number
    image: IPicture
    name: string
    updated: number
    url: string
    self: boolean
    voice: boolean
  }
}

export interface Person {
  id: number,
  url: string,
  name: string,
  country: {
    name: string,
    code: string,
    timezone: string,
  },
  birthday: string,
  deathday: string,
  gender: string,
  image: {
    medium: string,
    original: string,
  },
  updated: number,
  _links: {
    self: {
      href: string
    }
  }
}


export interface FeedBackDetails {
  fullName: string | null,
  email: string | null,
  content: string | null
}
