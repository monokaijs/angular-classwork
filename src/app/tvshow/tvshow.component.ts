import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ApiService} from "../api.service";
import {ICast, IShow} from "../../types";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-tvshow',
  templateUrl: './tvshow.component.html',
  styleUrls: ['./tvshow.component.scss']
})
export class TvshowComponent implements OnInit {
  public showName: string = '';
  public showImg: string = '';
  public showDescription: string = '';
  public cast: ICast[] = [];
  public filteredCast: ICast[] = [];
  public currentCaster: any = undefined;
  public showRating: number = 0;
  public isLoad: boolean = false;

  @Input() castQuery: string = '';
  @Input() showId: number = 0;

  constructor(private apiService : ApiService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
    // this.loadShow(13);
    const id = this.route.snapshot.params['id'];
    this.apiService.loadSingleShow(id).subscribe((data: IShow) => {
      this.showName = data.name;
      this.showDescription = data.summary;
      this.showImg = data.image.original || "";
      this.showRating = data.rating.average;
      this.isLoad = true;
    });
    this.apiService.loadShowCast(id).subscribe((data) => {
      this.cast = data;
      this.castQuery = '';
      this.filter();
    });
  }
  onSendRating($event: number) {
    this.showRating = $event;
    alert("Your rating is recorded.");
  }

  filter() {
    this.filteredCast = this.cast.filter(caster => (
      caster.person.name.toLowerCase().includes(this.castQuery.toLowerCase()) ||
      caster.character.name.toLowerCase().includes(this.castQuery.toLowerCase())
    ));
  }

  viewCaster(caster: ICast) {
    this.currentCaster = caster;
  }

}
