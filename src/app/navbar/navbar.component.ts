import {Component, Input, OnInit} from '@angular/core';
import {ApiService} from "../api.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  @Input() showId: number = 0;

  constructor(private apiService : ApiService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
  }

  loadShow(id: string) {
    this.router.navigate(['/show', id]).then(r => {
      console.log("ok");
    });
  }

}
