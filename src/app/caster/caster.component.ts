import {Component, OnInit} from '@angular/core';
import {ApiService} from "../api.service";
import {ActivatedRoute} from "@angular/router";
import {Person} from "../../types";

@Component({
  selector: 'app-caster',
  templateUrl: './caster.component.html',
  styleUrls: ['./caster.component.scss']
})
export class CasterComponent implements OnInit {
  public casterInfo: Person = {} as Person;
  public isLoad = false;

  constructor(private apiService: ApiService,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];
    this.apiService.loadCaster(id).subscribe((person: Person) => {
      this.isLoad = true;
      this.casterInfo = person;
      console.log(person);
    });
  }

}
