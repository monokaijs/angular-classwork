import {Component, OnInit} from '@angular/core';
import {FeedBackDetails} from "../../types";
import {NgForm, NgModel} from "@angular/forms";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  details: FeedBackDetails = {
    fullName: null,
    email: null,
    content: null
  }

  constructor() { }

  ngOnInit(): void {
  }

  onSubmit(myForm: NgForm) {
    console.log(myForm.form);
    console.log('Saved: ' + JSON.stringify(myForm.value));
    console.log(JSON.stringify(this.details));
  }
  isValidated(ctrl: NgModel): boolean | null {
    return ctrl.valid || (ctrl.pristine && ctrl.untouched);
  }
  getValidationClass(ctrl: NgModel): any {
    return {
      'is-valid': ctrl.touched
        && ctrl.value   // must have value in case no require validation
        // otherwise, touched and no value will show valid
        // which is not really nice
        && this.isValidated(ctrl),
      'is-invalid': !this.isValidated(ctrl)
    }
  }

  public checkInvalid(form: any) {
    const invalid = [];
    const controls = form.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    console.log(invalid);
    return invalid;
  }

}
