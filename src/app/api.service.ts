import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {ICast, IShow, Person} from "../types";

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  API_BASE : string = "https://api.tvmaze.com";
  constructor(private http: HttpClient) { }
  loadShows() {
    return this.http.get<IShow[]>(this.API_BASE + "/shows")
  }
  loadSingleShow(showId: number) {
    return this.http.get<IShow>(`${this.API_BASE}/shows/${showId}`);
  }
  loadShowCast(showId: number) {
    return this.http.get<ICast[]>(`${this.API_BASE}/shows/${showId}/cast`);
  }
  loadCaster(personId: number) {
    return this.http.get<Person>(`${this.API_BASE}/people/${personId}`);
  }
}
