import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {faStar, faStarHalfAlt} from '@fortawesome/free-solid-svg-icons';
import {faStar as faStarOutline} from "@fortawesome/free-regular-svg-icons";

@Component({
  selector: 'app-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.scss']
})
export class StarRatingComponent implements OnInit {
  @Input() rating: number = 0;
  public starHalf: number = 0;
  public starCount: number = 0;
  public noStarCount: number = 0;
  public currentDisplayRating: number = 0;

  @Output() ratingEvent = new EventEmitter<number>();

  constructor() {
  }

  ngOnInit(): void {
    this.currentDisplayRating = this.rating;
    this.resetStars();
  }

  sendRating(value: number) {
    this.ratingEvent.emit(value);
  }

  getIcon(index: number) {
    return index <= this.currentDisplayRating ? faStar : (
      Math.ceil(this.currentDisplayRating) === index ? faStarHalfAlt : faStarOutline
    )
  }

  mouseEnter(i: number) {
    this.currentDisplayRating = i;
  }

  mouseLeave(i: number) {
    this.currentDisplayRating = this.rating;
  }

  resetStars(): void {
    this.starCount = Math.floor(this.currentDisplayRating);
    this.starHalf = Math.floor(this.currentDisplayRating) != this.currentDisplayRating ? 1 : 0;
    this.noStarCount = 10 - this.starCount - this.starHalf;
  }

  getColor(i: number): string {
    return (i > 7) ? 'green' : (i > 5) ? 'orange' : 'red';
  }

  createRange(number: number) {
    return new Array(Math.floor(number));
  }
}
