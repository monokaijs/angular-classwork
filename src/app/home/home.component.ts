import {Component, Input, OnInit} from '@angular/core';
import {ApiService} from "../api.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  @Input() showId: number = 0;
  constructor(private apiService : ApiService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
  }


}
